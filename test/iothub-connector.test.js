'use strict'


//const should = require('should')

const amqp = require('amqplib')

let app = null
let _conn = null
let _channel = null

describe('IoTHub Connector> Test', () => {
  before('init', function () {
    process.env.ACCOUNT = 'demo.account'
    process.env.INPUT_PIPE = 'ip.iothub'
    process.env.LOGGERS = 'logger1, logger2'
    process.env.EXCEPTION_LOGGERS = 'ex.logger1, ex.logger2'
    process.env.BROKER = 'amqp://guest:guest@localhost'
    process.env.CONFIG = '{"protocol":"amqp"}'

    amqp.connect(process.env.BROKER)
      .then((conn) => {
        _conn = conn
        return conn.createChannel()
      }).then((channel) => {
        _channel = channel
      }).catch((err) => {
        console.log(err)
      })
  })

  after('close connection', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)
      app = require('../app')
      app.once('init', done)
    })
  })

  describe('#data', () => {
    it('should send data to third party client', function (done) {
      this.timeout(15000)

      let data = {
        device: 'Device100',
        temperature: 11,
        windSpeed: 14,
        humidity: 15,
        rkhDeviceInfo: {
          metadata: {
            azureConnectionString: 'HostName=Sales-Engineering-Testing.azure-devices.net;DeviceId=REEKOH-TEST1;SharedAccessKey=5OPjfkF1JGCv9huS/z96Rc9mzXCAZeiJYeTQkBqHsTc='
          }
        }
      }

      _channel.sendToQueue('ip.iothub', new Buffer(JSON.stringify(data)))
      setTimeout(done, 10000)
    })
  })
})

